import networkx as nx
import psycopg2
import os
from dotenv import load_dotenv


load_dotenv()

pg_conn = psycopg2.connect(
    dbname=os.getenv("POSTGRES_DB"),
    user=os.getenv("POSTGRES_LOGIN"),
    password=os.getenv("POSTGRES_PASS"),
    host=os.getenv("POSTGRES_HOST")
)
pg_cur = pg_conn.cursor()

pg_cur.execute("SELECT * FROM shipments")
shipments = pg_cur.fetchall()

pg_cur.execute("SELECT * FROM trucks")
trucks = pg_cur.fetchall()


    
for shipment in shipments:

    G = nx.Graph()

    G.add_node(shipment[-1], bipartite=0)

    for truck in trucks:

        G.add_node(truck[-1], bipartite=1)

        weight = 0

        # свободен ли грузовик
        if truck[-2] == "free":
            weight+=100

        # оптимальность маршрута
        truck_route = truck[2].split("-")
        shipment_route = shipment[2].split("-")
        if len(truck_route)==len(shipment_route) and truck_route[0]==shipment_route[0] \
            and truck_route[-1]==shipment_route[-1]:
            weight+=50
        elif truck_route[0]==shipment_route[0] \
            and truck_route[-1]==shipment_route[-1] and len(truck_route)!=len(shipment_route):
            route_weight = 50-10*(len(truck_route)-2)
            weight = route_weight if route_weight > 0 else 0

        # тип контейнера
        if truck[3]==shipment[3]:
            weight += 50

        # габариты и масса
        if truck[4]>=shipment[4] and truck[5]>=shipment[5] \
            and truck[6]>=shipment[6] and truck[7]>=shipment[7]:
            weight += 50

        # дата отправки и прибытия
        if truck[8] == shipment[8] and truck[10] <= shipment[10]:
            weight += 25

        # время отправки и время прибытия
        if truck[9] == shipment[9] and truck[11] <= shipment[11]:
            weight += 25  

        # тип загрузки
        if truck[12] == shipment[12]:
            weight += 50

        # регулярность
        if truck[13] == truck[13]:
            weight += 50

        # ценовой диапозон
        if shipment[14] <= truck[14] <= shipment[15]:
            weight += 60

        G.add_edge(shipment[-1], truck[-1], weight=weight) 

    nx.write_gml(G, f"./graphs/{shipment[-1]}_graph.gml")



        
