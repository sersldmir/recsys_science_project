import os
import logging
import json

from dotenv import load_dotenv
from pymongo import MongoClient

TRUCKS = "trucks"
SHIPMENTS = "shipments"

logging.basicConfig(level=logging.DEBUG, 
                    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')

load_dotenv()

logging.info("Connecting to mongo")
mongo_client = MongoClient(
    host=os.getenv("MONGO_HOST"),
    port=int(os.getenv("MONGO_PORT")),
)
logging.info("Connected to mongo")

mongo_db = mongo_client[os.getenv("MONGO_DB")]

recs = [f for f in os.listdir("./recs") if ".json" in f]
logging.info(f"Found files: {recs}")

for rec in recs:
    logging.info(f"Working with: {rec}")
    with open("./recs/" + rec, "r") as file:
        data = json.load(file)
        mongo_db["recs"].insert_one(data)
    logging.info(f"Sent {rec} to the database")

mongo_client.close()

logging.info("Connection closed")

