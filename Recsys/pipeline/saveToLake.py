import os
import psycopg2
import logging

from dotenv import load_dotenv
from pymongo import MongoClient

TRUCKS = "trucks"
SHIPMENTS = "shipments"

logging.basicConfig(level=logging.DEBUG, 
                    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')

load_dotenv()

logging.info("Connecting to mongo")
mongo_client = MongoClient(
    host=os.getenv("MONGO_HOST"),
    port=int(os.getenv("MONGO_PORT")),
)
logging.info("Connected to mongo")

mongo_db = mongo_client[os.getenv("MONGO_DB")]


pg_conn = psycopg2.connect(
    dbname=os.getenv("POSTGRES_DB"),
    user=os.getenv("POSTGRES_LOGIN"),
    password=os.getenv("POSTGRES_PASS"),
    host=os.getenv("POSTGRES_HOST")
)
pg_cur = pg_conn.cursor()

mongo_trucks_collection = mongo_db[TRUCKS]

trucks_data = mongo_trucks_collection.find()

logging.info("Getting trucks")
for truck_doc in trucks_data:
    logging.info("Inserting data")
    pg_cur.execute("INSERT INTO trucks (id_mongo, email, route, cont_type, length, \
                   width, height, ton, send_date, send_time, arr_date, arr_time, load_type,\
                   reg, price, write_date, write_time, status) VALUES\
                   (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",
                   (str(truck_doc["_id"]), truck_doc["email"], "-".join(truck_doc["route"]), truck_doc["cont_type"],
                   truck_doc["length"], truck_doc["width"], truck_doc["height"], truck_doc["ton"],
                   truck_doc["send_date"], truck_doc["send_time"], truck_doc["arr_date"], 
                   truck_doc["arr_time"], truck_doc["load_type"], truck_doc["reg"], truck_doc["price"],
                   truck_doc["write_date"], truck_doc["write_time"], truck_doc["status"]))
    logging.info("Data inserted")

mongo_shipments_collection = mongo_db[SHIPMENTS]

shipments_data = mongo_shipments_collection.find()

logging.info("Getting shipments")
for shipments_doc in shipments_data:
    logging.info("Inserting data")
    pg_cur.execute("INSERT INTO shipments (id_mongo, email, route, cont_type, length, \
                   width, height, ton, send_date, send_time, arr_date, arr_time, load_type,\
                   reg, min_price, max_price, write_date, write_time) VALUES\
                   (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",
                   (str(shipments_doc["_id"]), shipments_doc["email"], "-".join(shipments_doc["route"]), shipments_doc["cont_type"],
                   shipments_doc["length"], shipments_doc["width"], shipments_doc["height"], shipments_doc["ton"],
                   shipments_doc["send_date"], shipments_doc["send_time"], shipments_doc["arr_date"], 
                   shipments_doc["arr_time"], shipments_doc["load_type"], shipments_doc["reg"], shipments_doc["min_price"],
                   shipments_doc["max_price"], shipments_doc["write_date"], shipments_doc["write_time"]))
    logging.info("Data inserted")

pg_conn.commit()
pg_cur.close()
pg_conn.close()
mongo_client.close()

logging.info("Connection closed")

