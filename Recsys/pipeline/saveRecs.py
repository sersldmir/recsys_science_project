import networkx as nx
import os
import json

# загрузка графов
gml_files = [f for f in os.listdir("./graphs/") if ".gml" in f]

for gml_file in gml_files:

    G = nx.read_gml("./graphs/"+gml_file)

    pagerank_scores = nx.pagerank(G, alpha=0.85, 
                              weight="weight", tol=0.00001)
    
    recommendations = sorted(pagerank_scores.items(), 
                            key=lambda x: x[1], reverse=True)
    print("Рекомендуемые грузовики отправителю")
    i = 0
    shipment = ""
    trucks = []
    for truck, score in recommendations:
        if i==0:
            shipment = truck # первый элемент в графе - грузоотправитель, остальные - грузовики
            i+=1
            continue
        else:
            print(f"Грузовик: {truck}, PageRank значение: {score:.5f}")
            trucks.append(truck)
    rec = {shipment:trucks}
    with open(f"./recs/{shipment}_rec.json", "w") as file:
        json.dump(rec, file)
    
