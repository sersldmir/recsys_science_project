import pytest
import networkx as nx
import os


def graph_write():
    trucks = [(14, 'example1@mail.ru', 'Москва-Тверь-Казань', 'regular', 150, 90, 45, 295, '2023-12-08', '08:00', '2023-12-15', '18:00', 'боковая', 'false', 23000, '2023-12-06', '00:00', 'free', '656fe6c621f32801537b14af')]
    shipments = [(3, 'example2@mail.ru', 'Москва-Казань', 'refrigerator', 160, 95, 46, 297, '2023-12-07', '17:00', '2023-12-14', '20:00', 'задняя', 'false', 7000, 20000, '2023-12-06', '00:00', '656fe7bd21f32801537b14b0')]

    for shipment in shipments:

        G = nx.Graph()

        G.add_node(shipment[-1], bipartite=0)

        for truck in trucks:

            G.add_node(truck[-1], bipartite=1)

            weight = 0

            # свободен ли грузовик
            if truck[-2] == "free":
                weight+=100

            # оптимальность маршрута
            truck_route = truck[2].split("-")
            shipment_route = shipment[2].split("-")
            if len(truck_route)==len(shipment_route) and truck_route[0]==shipment_route[0] \
                and truck_route[-1]==shipment_route[-1]:
                weight+=50
            elif truck_route[0]==shipment_route[0] \
                and truck_route[-1]==shipment_route[-1] and len(truck_route)!=len(shipment_route):
                route_weight = 50-10*(len(truck_route)-2)
                weight = route_weight if route_weight > 0 else 0

            # тип контейнера
            if truck[3]==shipment[3]:
                weight += 50

            # габариты и масса
            if truck[4]>=shipment[4] and truck[5]>=shipment[5] \
                and truck[6]>=shipment[6] and truck[7]>=shipment[7]:
                weight += 50

            # дата отправки и прибытия
            if truck[8] == shipment[8] and truck[10] <= shipment[10]:
                weight += 25

            # время отправки и время прибытия
            if truck[9] == shipment[9] and truck[11] <= shipment[11]:
                weight += 25  

            # тип загрузки
            if truck[12] == shipment[12]:
                weight += 50

            # регулярность
            if truck[13] == truck[13]:
                weight += 50

            # ценовой диапозон
            if shipment[14] <= truck[14] <= shipment[15]:
                weight += 60

            G.add_edge(shipment[-1], truck[-1], weight=weight) 

    nx.write_gml(G, f"./Recsys/graphs/{shipment[-1]}_graph.gml")

def test_graph_presence():
    
    graph_write()
    files = os.listdir("./Recsys/graphs")

    assert len(files) > 0
