// client/src/App.js
import React, { useState } from 'react';
import LoginForm from './components/LoginForm';
import RegistrationForm from './components/RegistrationForm';
import Navbar from './components/Navbar';
import PersonalPage from './components/PersonalPage';
import { BrowserRouter as Router, Route, Routes, Navigate } from 'react-router-dom';

function App() {
    const [showLogin, setShowLogin] = useState(true);
    const [isAuthenticated, setIsAuthenticated] = useState(false);
    const [userType, setUserType] = useState('');
    const [loginError, setLoginError] = useState('');
    const [userLogin, setUserLogin] = useState('');


    return (
      <Router>
        {isAuthenticated ? (
          <>
          <Navbar userType={userType} />
          <Routes>
            <Route path="/personal"
            element={<PersonalPage 
              userLogin={userLogin} 
              setIsAuthenticated={setIsAuthenticated}/>} 
            />
            <Route path="/" element={<Navigate to="/personal"/>} />
          </Routes>
          </>
        ) : (
          <>
          <Routes>
            <Route path="/" element={
              <div className="container">
              <h1 style={{ textAlign: "center" }}>{showLogin ? 'Вход' : 'Регистрация'}</h1>
              {showLogin ? <LoginForm setUserLogin={setUserLogin} setIsAuthenticated={setIsAuthenticated} 
                        setUserType={setUserType} setLoginError={setLoginError} loginError={loginError}/> : 
              <RegistrationForm />}
              <br/>
              <div style={{ textAlign: "center" }}>
                  <button onClick={() => setShowLogin(!showLogin)} className="btn btn-secondary">
                  {showLogin ? 'Регистрация' : 'Вход'}
                  </button>
                </div>
              </div>
            }/>
          </Routes>
          </>
        )}
      </Router>
      
    );
  }

export default App;
