   // client/src/components/RegistrationForm.js
   import React, { useState } from 'react';
   import axios from 'axios';

   const RegistrationForm = () => {
     const [formData, setFormData] = useState({
       login: '',
       password: '',
       userType: 'sender' // default to 'seeker'
     });

     const { login, password, userType } = formData;

     const onChange = e => setFormData({ ...formData, [e.target.name]: e.target.value });

     const onSubmit = async e => {
       e.preventDefault();
       try {
         const config = {
           headers: {
             'Content-Type': 'application/json'
           }
         };
         const body = JSON.stringify({ login, password, userType });
         const res = await axios.post('/api/auth/register', body, config);
         console.log(res.data);
         // Handle registration success (e.g., save token, redirect, etc.)
       } catch (err) {
         console.error(err.response.data);
         // Handle errors (e.g., show error message)
       }
     };

     return (
       <form onSubmit={e => onSubmit(e)}>
         <div className="form-group">
           <input
             type="text"
             className="form-control"
             name="login"
             value={login}
             onChange={e => onChange(e)}
             placeholder="Login"
             required
           />
         </div>
         <div className="form-group">
           <input
             type="password"
             className="form-control"
             name="password"
             value={password}
             onChange={e => onChange(e)}
             placeholder="Password"
             required
           />
         </div>
         <div className="form-group">
           <select
             name="userType"
             value={userType}
             onChange={e => onChange(e)}
             className="form-control"
           >
             <option value="sender">Грузоотправитель</option>
             <option value="shipper">Грузоперевозчик</option>
           </select>
         </div>
         <div style={{ textAlign: "center" }}>
            <button type="submit" className="btn btn-primary">Регистрация</button>
         </div>
       </form>
     );
   };

   export default RegistrationForm;
