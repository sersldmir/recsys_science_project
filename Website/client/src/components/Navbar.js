// client/src/components/Navbar.js
import React from 'react';
import { Link } from 'react-router-dom';

const Navbar = ({ userType }) => {
    const sections = userType === 'sender' ? ['recommendations', 'orders', 'personal'] : ['trucks', 'orders', 'personal'];

    return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
        <Link className="navbar-brand" to="/">Delivery Service</Link>
        <div className="collapse navbar-collapse">
        <ul className="navbar-nav ml-auto">
            {sections.map(section => (
            <li key={section} className="nav-item">
                <Link className="nav-link" to={`/${section}`}>{section.charAt(0).toUpperCase() + section.slice(1)}</Link>
            </li>
            ))}
        </ul>
        </div>
    </nav>
    );
};

export default Navbar;
