   // client/src/components/LoginForm.js
   import React, { useState } from 'react';
   import axios from 'axios';

   const LoginForm = ({ setUserLogin, setIsAuthenticated, setUserType, setLoginError, loginError }) => {
     const [formData, setFormData] = useState({
       login: '',
       password: ''
     });

     const { login, password } = formData;

     const onChange = e => setFormData({ ...formData, [e.target.name]: e.target.value });

     const onSubmit = async e => {
       e.preventDefault();
       try {
         const config = {
           headers: {
             'Content-Type': 'application/json'
           }
         };
         const body = JSON.stringify({ login, password });
         const res = await axios.post('/api/auth/login', body, config);
         setIsAuthenticated(true);
         setUserType(res.data.userType); // Set the user type
         setUserLogin(res.data.login); // Set the login of the user
         localStorage.setItem('token', JSON.stringify(res.data.token)) // Set token to local storage
         setLoginError(''); // Clear any existing errors
         // Handle authentication success (e.g., save token, redirect, etc.)
       } catch (err) {
         console.error(err.response.data);
         setIsAuthenticated(false);
         setUserType('');
         setLoginError('Incorrect login or password');
         // Handle errors (e.g., show error message)
       }
     };

     return (
       <form onSubmit={e => onSubmit(e)}>
        {loginError && <div className="alert alert-danger">{loginError}</div>}
         <div className="form-outline mb-4">
           <input
             type="text"
             id="login"
             className="form-control"
             name="login"
             value={login}
             onChange={e => onChange(e)}
             required
           />
           <label className="form-label" htmlFor="login">Логин</label>
         </div>
         <div className="form-group">
           <input
             type="password"
             id="pass"
             className="form-control"
             name="password"
             value={password}
             onChange={e => onChange(e)}
             required
           />
           <label className="form-label" htmlFor="pass">Пароль</label>
         </div>
         <div style={{ textAlign: "center" }}>
            <button type="submit" className="btn btn-primary">Войти</button>
         </div>
       </form>
     );
   };

   export default LoginForm;
