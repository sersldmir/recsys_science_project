   // client/src/components/PersonalPage.js
   import React, { useState } from 'react';
   import axios from 'axios';
   import { useNavigate } from 'react-router-dom';

   const PersonalPage = ({ userLogin, setIsAuthenticated }) => {
     const [password, setPassword] = useState('');
     const history = useNavigate();

     const onChangePassword = (e) => {
       setPassword(e.target.value);
     };

     const onChangePasswordSubmit = async (e) => {
        e.preventDefault();
        try {
          const config = {
            headers: {
              'Content-Type': 'application/json',
              'x-auth-token': JSON.parse(localStorage.getItem('token')), // Assuming you store the token in localStorage
            },
          };
          const body = JSON.stringify({ password });
          await axios.put('/api/user/password', body, config);
          setIsAuthenticated(false);
          localStorage.removeItem('token'); // Remove the token from localStorage
          history("/");
        } catch (err) {
          console.error(err);
          // Handle errors (e.g., show error message)
        }
     };

     const onDeleteAccount = async () => {
        try {
            const config = {
              headers: {
                'Content-Type': 'application/json',
                'x-auth-token': JSON.parse(localStorage.getItem('token')), // Assuming you store the token in localStorage
              },
            };
            await axios.delete('/api/user', config);
            localStorage.removeItem('token'); // Remove the token from localStorage
            setIsAuthenticated(false);
            history("/"); // Redirect to the login page
          } catch (err) {
            console.error(err);
            // Handle errors (e.g., show error message)
          }
     };

     return (
       <div>
         <h2>Personal Page</h2>
         <p>Login: {userLogin}</p>
         <form onSubmit={onChangePasswordSubmit}>
           <input type="password" value={password} onChange={onChangePassword} />
           <button type="submit">Change Password</button>
         </form>
         <button onClick={onDeleteAccount}>Delete Account</button>
       </div>
     );
   };

   export default PersonalPage;
