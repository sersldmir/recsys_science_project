const dotenv = require('dotenv')
const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const cors = require('cors');
const authRoutes = require('./src/routes/auth');
const userRoutes = require('./src/routes/user');

// Init
const app = express();
dotenv.config();

// Middleware
app.use(bodyParser.json());
app.use(cors());

// Connection
mongoose.connect(process.env.MONGO, { useNewUrlParser: true, useUnifiedTopology: true })
  .then(() => console.log('MongoDB connected...'))
  .catch(err => console.log(err));

// Use Routes
app.use('/api/src/auth', authRoutes);
app.use('/api/src/user', userRoutes);

// Listening
const PORT = 5000;
app.listen(PORT, () => console.log(`Server started on port ${PORT}`));
