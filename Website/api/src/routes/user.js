   // routes/user.js
   const express = require('express');
   const router = express.Router();
   const bcrypt = require('bcrypt');
   const auth = require('../middleware/auth'); // Ensure you have authentication middleware
   const User = require('../models/User');

   // @route   PUT api/user/password
   // @desc    Update user password
   // @access  Private
   router.put('/password', auth, async (req, res) => {
     const { password } = req.body;
     try {
       const salt = await bcrypt.genSalt(10);
       const newPassword = await bcrypt.hash(password, salt);

       await User.findByIdAndUpdate(req.user.id, { password: newPassword });
       res.json({ msg: 'Password updated successfully' });
     } catch (err) {
       console.error(err.message);
       res.status(500).send('Server error');
     }
   });

   // @route   DELETE api/user
   // @desc    Delete user account
   // @access  Private
   router.delete('/', auth, async (req, res) => {
     try {
       await User.findByIdAndDelete(req.user.id);
       res.json({ msg: 'User deleted successfully' });
     } catch (err) {
       console.error(err.message);
       res.status(500).send('Server error');
     }
   });

   module.exports = router;
