// routes/auth.js
const express = require('express');
const dotenv = require('dotenv');
const router = express.Router();
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const User = require('../models/User');

dotenv.config();

// Register route
router.post('/register', async (req, res) => {
    try {
    const { login, password, userType } = req.body;

    // Check if user exists
    let user = await User.findOne({ login });
    if (user) {
        return res.status(400).json({ msg: 'User already exists' });
    }

    // Create a new user
    user = new User({
        login,
        password,
        userType
    });

    // Encrypt password
    const salt = await bcrypt.genSalt(10);
    user.password = await bcrypt.hash(password, salt);

    // Save user to the database
    await user.save();

    // Return jsonwebtoken
    const payload = {
        user: {
        id: user.id
        }
    };

    jwt.sign(
        payload,
        process.env.JWT_SECRET, // Add a JWT_SECRET in your .env file
        { expiresIn: 3600 },
        (err, token) => {
        if (err) throw err;
        res.json({ token, userType: user.userType, login: user.login});
        }
    );
    } catch (err) {
    console.error(err.message);
    res.status(500).send('Server error');
    }
});

// Login route
router.post('/login', async (req, res) => {
    const { login, password } = req.body;

    try {
    // Check for user
    let user = await User.findOne({ login });
    if (!user) {
        return res.status(400).json({ msg: 'Invalid Credentials' });
    }

    // Validate password
    const isMatch = await bcrypt.compare(password, user.password);
    if (!isMatch) {
        return res.status(400).json({ msg: 'Invalid Credentials' });
    }

    // Return jsonwebtoken
    const payload = {
        user: {
        id: user.id
        }
    };

    jwt.sign(
        payload,
        process.env.JWT_SECRET,
        { expiresIn: 3600 },
        (err, token) => {
        if (err) throw err;
        res.json({ token, userType: user.userType, login: user.login});
        }
    );
    } catch (err) {
    console.error(err.message);
    res.status(500).send('Server error');
    }
});

module.exports = router;
