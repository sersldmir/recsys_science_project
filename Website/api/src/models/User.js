// models/User.js
const mongoose = require('mongoose');

const UserSchema = new mongoose.Schema({
    login: {
    type: String,
    required: true,
    unique: true
    },
    password: {
    type: String,
    required: true
    },
    userType: {
    type: String,
    required: true,
    enum: ['sender', 'shipper']
    }
});

module.exports = mongoose.model('User', UserSchema);
